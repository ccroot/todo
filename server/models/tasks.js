var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TaskSchema = new Schema({
  name: String,
  created: Date,
  status: Boolean,
  listId: String
});

module.exports = mongoose.model('Task', TaskSchema);
