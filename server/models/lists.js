var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ListSchema = new Schema({
  name: String,
  created: Date
});

module.exports = mongoose.model('List', ListSchema);
