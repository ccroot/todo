export class ListModel {
    name: string;
    created: number;
    listId: string;

    constructor(name) {
        this.name = name;
        this.created = Date.now();
        this.listId = this.name + this.created;
    }
}
