export class TaskModel {
  _id: string;
    name: string;
    created: number;
    status: boolean;
    listId: string;

    constructor(name: string, listId: string) {
        this.name = name;
        this.created = Date.now();
        this.status = false;
        this.listId = listId;
    }
}
