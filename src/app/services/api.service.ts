import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {MessagesService} from './messages.service';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ApiService {

    private apiUrl = 'http://localhost:8080/api';

    private dbjMessage(msg) {
        this.msgService.errorMsg(msg);
    }

    constructor(
      private httpService: HttpClient,
      private msgService: MessagesService
    ) {}

    api(method: string, patch: string, body) {
      return this.httpService
        [ method ](`${this.apiUrl}/${patch}`, body ? body : null)
        .toPromise()
        .catch((err) => {
          this.dbjMessage(err);
        });
    }

}
