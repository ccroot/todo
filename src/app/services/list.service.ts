import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ApiService} from './api.service';


@Injectable()
export class ListService {

    private _lists: BehaviorSubject<any>;

    constructor(private apiService: ApiService) {
        this._lists = new BehaviorSubject<any>([]);
        this.apiService
          .api('get', 'lists', {})
          .then(lists => this._lists.next(lists))
          .catch();
    }

    get list() {
        return this._lists.asObservable();
    }

    addList(name: string) {
      this.apiService
        .api('post', 'lists', {name: name} )
        .then((list) => {
          this._lists
            .next([...this._lists.getValue(), list]);
        })
        .catch();
    }


    delList(id) {
        this.apiService
            .api('delete', `lists/${id}`, {})
            .then(() => {
              this._lists
                .next(this._lists.getValue().filter(lists => lists._id !== id));
            })
          .catch();
    }

}



