import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ApiService} from './api.service';
import {TaskModel} from '../models/task-model';

@Injectable()
export class TaskService {

  private _tasks: BehaviorSubject<any>;

  constructor(private apiService: ApiService) {
    this._tasks = new BehaviorSubject<any>([]);
  }

  getTasks(id) {
    this.apiService
      .api('get', `tasks/listid/${id}`, {})
      .then(tasks => this._tasks.next(tasks))
      .catch();
    return this._tasks.asObservable();

  }

  addTask(name: string, listid: string) {
    this.apiService
      .api('post', 'tasks', {name: name, listId: listid})
      .then((task) => this._tasks.next([...this._tasks.getValue(), task]))
      .catch();
  }

  deleteTask(id) {
    this.apiService
      .api('delete', `tasks/${id}`, {})
      .then(() => {
          this._tasks.next(this._tasks.getValue().filter(tasks => tasks._id !== id));
        })
      .catch();
  }

  switchTaskStatus(task: TaskModel) {
    !task.status ? task.status = true : task.status = false;
    this.apiService
      .api('put', `tasks/${task._id}`, task)
      .then()
      .catch();
    }

}
