import {Injectable, OnInit} from '@angular/core';
import {ListService} from './list.service';
import {TaskService} from './task.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ListModel} from '../models/list-model';
import {TaskModel} from '../models/task-model';



@Injectable()
export class UserNotificationService implements OnInit {

  private _lists: BehaviorSubject<ListModel>;
  private _tasks: BehaviorSubject<TaskModel>;

  constructor (
    private listService: ListService,
    private taskService: TaskService
  ) {}

  ngOnInit() {
    this.listService.list.subscribe(lists => this._lists = lists);
    console.log('w');
  }

}
