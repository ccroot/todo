import {Component, Input, OnInit} from '@angular/core';
import {TaskService} from '../../services/task.service';
import {ListService} from '../../services/list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  providers: [TaskService],
})
export class ListComponent implements OnInit {
  @Input() list: any;
  private tasks: any = [];

  deleteList(id) {
    this.listService.delList(id);
  }

  constructor (
      private taskService: TaskService,
      private listService: ListService
  ) {}

  ngOnInit() {
    this.taskService.getTasks(this.list._id)
      .subscribe(tasks => this.tasks = tasks);
  }
}


