import {Component} from '@angular/core';
import {ListService} from '../../services/list.service';

@Component({
  selector: 'div[type=addform]',
  templateUrl: './list-addform.component.html',
})
export class ListAddFormComponent {

  constructor(private listService: ListService) {
  }

  addList(name: string) {
    name && name.trim().length > 2 && name.trim().length < 25 ?
      this.listService.addList(name.trim()) : console.log('addform corrupted');
  }

}
