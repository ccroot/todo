import {Component, EventEmitter, Input, Output} from '@angular/core';
import {TaskService} from '../../services/task.service';

@Component({
  selector: 'app-task-addform',
  templateUrl: './task-addform.component.html'
})
export class TaskAddFormComponent {
  @Input()
  listId: string;

  constructor (private taskService: TaskService) {}

  addTask(name: string) {
    name && name.trim().length > 2 && name.trim().length < 25 ?
      this.taskService.addTask(name.trim(), this.listId) : console.log('taskform error');
  }

}
