import {Component, EventEmitter, Input, Output} from '@angular/core';
import {TaskService} from '../../services/task.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
})
export class TaskComponent {
  @Input() task: any;
  @Output()
  taskChanger: EventEmitter<any> = new EventEmitter();

  constructor (private taskService: TaskService) {}

  deleteTask(id) {
    this.taskService.deleteTask(id);
    this.taskChanger.emit('');
  }

  switchTaskStatus(task) {
    this.taskService.switchTaskStatus(task);
    this.taskChanger.emit('');
  }

}


