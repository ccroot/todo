import {Component, OnInit} from '@angular/core';
import {ListService} from '../services/list.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [ListService],
})
export class AppComponent implements OnInit {
  private lists = [];

  constructor(private listService: ListService) {}

  listsUpdater(lists) {
    this.lists = lists;
  }

  ngOnInit() {
    this.listService.list.subscribe(lists => this.listsUpdater(lists),
      console.log);
  }


}
