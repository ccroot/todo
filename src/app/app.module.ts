import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './components/app.component';
import { ListComponent } from './components/list/list.component';
import { TaskComponent } from './components/task/task.component';
import { ListService } from './services/list.service';
import { ListAddFormComponent } from './components/list/list-addform.component';
import { TaskAddFormComponent } from './components/task/task-addform.component';
import {ApiService} from './services/api.service';
import {MessagesService} from './services/messages.service';
import {HttpClientModule} from '@angular/common/http';
import {UserNotificationService} from './services/user.notification.service';


@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    TaskComponent,
    ListAddFormComponent,
    TaskAddFormComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule
  ],
  providers: [ListService, ApiService, MessagesService, UserNotificationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
