var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var mongoose   = require('mongoose');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

var port = process.env.PORT || 8080;
mongoose.connect('mongodb://todoAdmin:flvbybcnhfnjhpflfxm@ds119988.mlab.com:19988/tododb');

var List = require('./server/models/lists.js');
var Task = require('./server/models/tasks.js');

var router = express.Router();

router.get('/', function(req, res) {
  res.json({ message: 'ToDo App Api' });
});

router.route('/lists')
  .post(function (req, res) {
    if (req.body.name.trim().length > 2 && req.body.name.trim().length < 25) {
      var list = new List();

      list.name = req.body.name;
      list.created = Date.now();

      list.save(function(err) {
        if (err)
          res.send(err);
        res.json(list)
      })

    } else {
      console.log('error');
    }
  })

  .get(function(req, res) {
    List.find(function(err, lists) {
      if (err)
        res.send(err);

      res.json(lists);
    });
  });


router.route('/wipetasks')
  .get(function(req, res) {
    Task.remove({}, function(err) {
      if (err)
        res.send(err);

      res.json({ message: 'Successfully deleted all' });
    });
  });

router.route('/tasks')
  .post(function (req, res) {
    if (req.body.listId && req.body.name.trim().length > 2 && req.body.name.trim().length < 25) {
      var task = new Task();
      task.name = req.body.name;
      task.created = Date.now();
      task.status = false;
      task.listId = req.body.listId;

      task.save(function(err) {
        if (err)
          res.send(err);
        res.json(task)
      })
    } else {
      console.log('smbd try to do bad action');
      res.send('error')
    }
  })

  .get(function(req, res) {
    Task.find(function(err, tasks) {
      if (err)
        res.send(err);

      res.json(tasks);
    });
  });
router.route('/tasks/listid/:listId')
  .get(function(req, res) {
    Task.find({ listId: req.params.listId}, function(err, tasks) {
      if (err)
        res.send('works' + err);
      res.json(tasks);
    });
  });

router.route('/tasks/:task_id')

  .get(function(req, res) {
    Task.findById(req.params.task_id, function(err, task) {
      if (err)
        res.send(err);
      res.json(task);
    });
  })
  .put(function(req, res) {
    Task.findById(req.params.task_id, function(err, task) {

      if (err)
        res.send(err);

      task.name = req.body.name;
      task.created = req.body.created;
      task.status = req.body.status;
      task.listId = req.body.listId;

      task.save(function(err) {
        if (err)
          res.send(err);

        res.json({ message: 'task updated!' });
      });

    });
  })
  .delete(function(req, res) {
    Task.remove({
      _id: req.params.task_id
    }, function(err) {
      if (err)
        res.send(err);

      res.json({ message: 'Successfully deleted' });
    });
  });

router.route('/lists/name/:name')
  .get(function(req, res) {
    List.findOne({ name: req.params.name}, function(err, list) {
      if (err)
        res.send(err);
      res.json(list);
    });
  });

router.route('/lists/:list_id')

  .get(function(req, res) {
    List.findById(req.params.list_id, function(err, list) {
      if (err)
        res.send(err);
      res.json(list);
    });
  })
  .delete(function(req, res) {
    List.remove({
      _id: req.params.list_id
    }, function(err) {
      if (err)
        res.send(err);


      Task.remove({listId: req.params.list_id}, function (err) {
        if (err) {
          return res.send(err)
        }

        res.json({ message: 'Successfully deleted' });
      })
    });
  });



app.use('/api', router);



app.listen(port);
console.log('Api server start at port: ' + port);
